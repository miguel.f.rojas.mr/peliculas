import { Component, OnInit } from "@angular/core";
import { MoviesService } from "../services/movies.service";
import { Pelicula } from "../interfaces/interfaces";
import { ModalController } from "@ionic/angular";
import { DetalleComponent } from "../components/detalle/detalle.component";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page implements OnInit {
  textoBuscar = "";
  peliculas: Pelicula[] = [];
  recomendaciones: Pelicula[] = [];
  buscando = false;

  constructor(
    private movieService: MoviesService,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.cargarDatos();
  }

  buscar(event) {
    const valor = event.detail.value;
    this.buscando = true;
    console.log(event.detail.value);
    if (valor) {
      this.movieService.getBuscarPelicula(valor).subscribe(resp => {
        console.log(resp);
        this.peliculas = resp["results"];
        this.buscando = false;
      });
    } else {
      this.buscando = false;
      this.peliculas = [];
    }
  }

  cargarDatos() {
    this.movieService.getPopulares().subscribe(resp => {
      this.recomendaciones = resp.results;
    });
  }

  async verDetalle(id: string) {
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: {
        id
      }
    });
    modal.present();
  }
}
